#! /bin/bash -f

sizes=( "360" "480" "720" "1080" "1920" )
palette="/tmp/palette.png"
for i in "${sizes[@]}"; do
	filters="fps=30,scale=$i:-1:flags=lanczos"
	ffmpeg -v warning -i $1 -vf "$filters,palettegen" -y $palette
	ffmpeg -v warning -i $1 -i $palette -loop -1 -lavfi "$filters [x]; [x][1:v] paletteuse" -y $2-$i.gif
done